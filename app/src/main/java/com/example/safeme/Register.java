package com.example.safeme;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

public class Register extends AppCompatActivity {

    Button btnCancel, btnRegister;
    TextView txtName, txtPassword, txtEmail, txtContacto;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btnCancel = findViewById(R.id.btnCancel);
        btnRegister = findViewById(R.id.btnRegister);
        txtName = findViewById(R.id.txtName);
        txtPassword = findViewById(R.id.txtPassword);
        txtEmail = findViewById(R.id.txtEmail);
        txtContacto = findViewById(R.id.txtContact);
        firebaseAuth = FirebaseAuth.getInstance();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = txtEmail.getText().toString().trim();
                String password = txtPassword.getText().toString().trim();
                final String name = txtName.getText().toString();
                final String contact = txtContacto.getText().toString();

                if(TextUtils.isEmpty(email)){
                    Toast.makeText(Register.this,"Correo No Valido", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    Toast.makeText(Register.this,"Contraseña No Valida", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(name)){
                    Toast.makeText(Register.this,"Nombre No Valido", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(contact)){
                    Toast.makeText(Register.this,"Contacto No Valido", Toast.LENGTH_SHORT).show();
                    return;
                }

                firebaseAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    //registramos en la BD
                                    FirebaseUser userSession = FirebaseAuth.getInstance().getCurrentUser();
                                    mDatabase = FirebaseDatabase.getInstance().getReference();
                                    mDatabase.child("User").child(userSession.getUid()).child("name").setValue(name);
                                    mDatabase.child("User").child(userSession.getUid()).child("contacts").setValue(contact);
                                    mDatabase.child("User").child(userSession.getUid()).child("email").setValue(email);
                                    startActivity(new Intent(getApplicationContext(),deshboard.class));
                                    //Toast.makeText(Register.this,"Registro exitoso", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(Register.this,"Algo salio mal, vuelve a intentarlo", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });

    }
}
