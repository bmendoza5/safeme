package com.example.safeme;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import android.widget.Button;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.safeme.model.User;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Random;



public class askFinishData extends AppCompatActivity implements OnMapReadyCallback {

    private Button startTrackingBtn, btnCancel;
    private TextView txtEndHour, txtEndDate;
    private DatabaseReference mDatabase;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private static final int PERMISSIONS_REQUEST = 100;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    private ConstraintLayout mainContainer;
    private MapView mapView;
    private GoogleMap gmap;
    private TextView lblEndPoint;

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_finish_data);


        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        initializeViewVariables(mapViewBundle);
        //startTrackerService();
        LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        startTrackingBtn = findViewById(R.id.startTracking);
        startTrackingBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //inTracking();--regresar a esta funcion
                //addCurrentLocation();--prueva, guardado de varias ubicaciones
                startTracking();
            }
        });
        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),deshboard.class));
            }
        });

        //FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


        if (!locationPermissionionIsEnable()) {
            requestPermissions();
        }

    }

    private void initializeViewVariables(Bundle mapViewBundle) {
        mainContainer = findViewById(R.id.mainContainer);
        startTrackingBtn = findViewById(R.id.startTracking);
        lblEndPoint = findViewById(R.id.lblEndPoint);
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(com.example.safeme.askFinishData.this);

    }

    public void inTracking(View view) {
        Intent inTrackingActicity = new Intent(this, inTracking.class);
        startActivity(inTrackingActicity);
        inicializeFirebase();

    }

    public void inicializeFirebase() {
        FirebaseApp.initializeApp(this);
        firebaseDatabase = firebaseDatabase.getInstance();
        databaseReference = databaseReference.getRef();
    }


    private boolean locationPermissionionIsEnable() {
        int fine_location_permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarse_location_permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        return (fine_location_permission == PackageManager.PERMISSION_GRANTED &&
                coarse_location_permission == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSIONS_REQUEST);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            Snackbar.make(mainContainer,
                    "Es necesario el permiso de localización.",
                    Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(Color.RED)
                    .setAction(android.R.string.ok,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //ABRIR CONFIGURACION
                                    openAppConfig();
                                }
                            })
                    .show();
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(final GoogleMap googleMap) {

        if (!locationPermissionionIsEnable()) {
            return;
        }

        LocationRequest request = new LocationRequest();
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        request.setInterval(60*1000);

        googleMap.setMyLocationEnabled(true);

        client.requestLocationUpdates(request, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location location = locationResult.getLastLocation();
                gmap = googleMap;
                LatLng actualPosition = new LatLng(location.getLatitude(), location.getLongitude());
                gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(actualPosition, 14.0f));
            }
        }, null);

        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng position) {
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions()
                        .position(position)
                        .title("Destino")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));


                Geocoder geocoder;
                geocoder = new Geocoder(askFinishData.this, Locale.getDefault());
                List<Address> addresses;

                try {
                    addresses = geocoder.getFromLocation(position.latitude, position.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address = addresses.get(0).getAddressLine(0);
                    lblEndPoint.setText(getString(R.string.endPoint) + address);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                
            }
        });

    }


    public void openAppConfig(){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    private void startTrackerService() {
        startService(new Intent(this, TrackingService.class));
        Toast.makeText(this, "GPS tracking enabled", Toast.LENGTH_SHORT).show();

    }


    public void inTracking(){
        txtEndDate = findViewById(R.id.txtEndDate);
        txtEndHour = findViewById(R.id.txtEndHour);
        FirebaseUser userSession = FirebaseAuth.getInstance().getCurrentUser();

        User user = new User();
        user.setId(userSession.getUid());
        user.setName(userSession.getEmail());
        //String[] contactos = new String[2];
        //contactos[0]= "3123123";
        //contactos[1]= "121212";
        //user.setContacts("correo@algo.com");
        user.setEndLongitud("15.3422");//la que eligio de fin
        user.setEndLatitud("-113.342");//de finalizacion

        //registra el tracking en la bd
        mDatabase = FirebaseDatabase.getInstance().getReference(); //On Create
        //mDatabase.child("User").setValue(user); //Donde envías el dato
        Toast.makeText(this,"Succesful", Toast.LENGTH_SHORT).show();
        mDatabase.child("User").child(userSession.getUid()).child("Tracking").setValue("cordenadas{lat='11.121',long='12.22}");

        startActivity(new Intent(getApplicationContext(),inTracking.class));
    }

    public void addCurrentLocation(){
        //TODO Asi se guarda el proceso de tracking para que funcione
        FirebaseUser userSession = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        Random aleatorio = new Random(System.currentTimeMillis());
        int intAletorio = aleatorio.nextInt(100);
        mDatabase.child("User").child(userSession.getUid()).child("Tracking").child("tract_"+intAletorio).child("latitud").setValue("12.2322");
        mDatabase.child("User").child(userSession.getUid()).child("Tracking").child("tract_"+intAletorio).child("longitud").setValue("13.2322");
        Toast.makeText(this,"Succesful", Toast.LENGTH_SHORT).show();
    }

    public void startTracking(){
        FirebaseUser userSession = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        txtEndDate = findViewById(R.id.txtEndDate);
        txtEndHour = findViewById(R.id.txtEndHour);
        String endDate = txtEndDate.getText().toString();
        String endHour = txtEndHour.getText().toString();
        //TODO reemplazar por coordenadas de finalizacion
        mDatabase.child("User").child(userSession.getUid()).child("finishedTracking").child("latitud").setValue("12.2322");
        mDatabase.child("User").child(userSession.getUid()).child("finishedTracking").child("longitud").setValue("13.3333");
        mDatabase.child("User").child(userSession.getUid()).child("finishedTracking").child("hour").setValue(endHour);
        mDatabase.child("User").child(userSession.getUid()).child("finishedTracking").child("date").setValue(endDate);

        startActivity(new Intent(getApplicationContext(),inTracking.class));
    }
}
