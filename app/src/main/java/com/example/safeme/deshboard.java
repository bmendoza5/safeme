package com.example.safeme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class deshboard extends AppCompatActivity {

    TextView txtUserName, txtContact;
    Button btnStart, btnLogout, btnAddContact;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deshboard);

        txtUserName = findViewById(R.id.txtUserName);
        btnStart = findViewById(R.id.btnStart);
        btnLogout = findViewById(R.id.btnLogout);
        btnAddContact = findViewById(R.id.btnAddContacto);
        txtContact = findViewById(R.id.txtContact);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        txtUserName.setText("Hola" +"");//traerse el nombre de la bd
        /*if (user != null) {
            Toast.makeText(this,"tenemos usuario", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this,"algo anda mal", Toast.LENGTH_SHORT).show();
        }*/
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),askFinishData.class));
            }
        });
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getApplicationContext(),Login.class));
            }
        });
        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !(TextUtils.isEmpty(txtContact.getText().toString())) ){
                    mDatabase = FirebaseDatabase.getInstance().getReference(); //On Create
                    //mDatabase.child("User").child(user.getUid()).child("Contacts").child(txtContact.getText().toString()).setValue(txtContact.getText().toString());
                    mDatabase.child("User").child(user.getUid()).child("contacts").setValue(txtContact.getText().toString());
                    txtContact.setText("");
                    Toast.makeText(deshboard.this,"Guardado", Toast.LENGTH_SHORT).show();
                }else{
                    return;
                }

            }
        });
    }
}
