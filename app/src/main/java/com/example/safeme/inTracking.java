package com.example.safeme;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class inTracking extends AppCompatActivity {

    private Button btnCanceel;
    private DatabaseReference mDatabase;
    private static final int PERMISSIONS_REQUEST = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_tracking);

        //ligar con un text view para recuperar el dato
        String dato = getIntent().getStringExtra("variable");
        //textvariable.setText(dato);

        btnCanceel = findViewById(R.id.btnCancel);
        btnCanceel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cancelTracking();
                //example();
                Date date1 = Calendar.getInstance().getTime();
                Date date2 = Calendar.getInstance().getTime();
                long diferencia = (Math.abs(date1.getTime() - date2.getTime())) / 1000;
                long limit = (60 * 1000) / 1000L;//limite de tiempo

                if (diferencia <= limit) {
                    Log.e("hora----->","no se pero si hay diferencia"+diferencia);
                }else{
                    Log.e("hora----->", String.valueOf(diferencia));
                }

                if(ActivityCompat.checkSelfPermission(inTracking.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED&& ActivityCompat.checkSelfPermission(inTracking.this,Manifest.permission.SEND_SMS)!= PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(inTracking.this,new String[]{ Manifest.permission.SEND_SMS,},1000);
                }else{
                    enviarMensaje("3125951050","Esta es una prueba de bryan y su nueva app");
                }

            }
        });
    }

    private void enviarMensaje (String numero, String mensaje){
        try {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(numero,null,mensaje,null,null);
            Toast.makeText(getApplicationContext(), "Mensaje Enviado.", Toast.LENGTH_LONG).show();
        }
        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Mensaje no enviado, datos incorrectos.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void SacarDatos_de_la_BD(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase.child("User").child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String endHour = dataSnapshot.child("finishedTracking").child("hour").getValue().toString();
                    Log.e("dato---->",endHour);
                }
                Log.e("captura---->",dataSnapshot.toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }

    private void cancelTracking(){
        Intent cancel = new Intent(this, deshboard.class);
        startActivity(cancel);
    }
}
