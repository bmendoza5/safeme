package com.example.safeme.model;

import java.util.Arrays;

public class User {
    private String id;
    private String name;
    private String[] Contacts;
    private String endLatitud;
    private String endLongitud;
    private String[] tracking;

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getContacts() {
        return Contacts;
    }

    public void setContacts(String[] contacts) {
        Contacts = contacts;
    }

    public String getEndLatitud() {
        return endLatitud;
    }

    public void setEndLatitud(String endLatitud) {
        this.endLatitud = endLatitud;
    }

    public String getEndLongitud() {
        return endLongitud;
    }

    public void setEndLongitud(String endLongitud) {
        this.endLongitud = endLongitud;
    }

    public String[] getTracking() {
        return tracking;
    }

    public void setTracking(String[] tracking) {
        this.tracking = tracking;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", Contacts=" + Arrays.toString(Contacts) +
                ", endLatitud='" + endLatitud + '\'' +
                ", endLongitud='" + endLongitud + '\'' +
                ", tracking='" + tracking + '\'' +
                '}';
    }
}
